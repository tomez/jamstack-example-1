module.exports = function(config) {

    config.setBrowserSyncConfig({
        https: {
            key: './server.key',
            cert: './server.crt'
        }
    });

    config.addPassthroughCopy("src/js");
    return {
        dir: {
            input: "src",
            output: "dist",
            data: "_data"
        }
    };

};